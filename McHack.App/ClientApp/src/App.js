import React, { Component } from 'react';
import { Route } from 'react-router';
import { FetchData } from './components/FetchData';
import { MainFiltration } from '../src/components/MainFiltration';

import './custom.css';
import 'antd/dist/antd.min.css';
import 'devextreme/dist/css/dx.light.css';

import deMessages from "devextreme/localization/messages/de.json";
import ruMessages from "devextreme/localization/messages/ru.json";
import { locale, loadMessages } from "devextreme/localization";

import { isExpired } from "react-jwt";

import AuthenticateForm from './components/AuthenticateForm';
import Authenticator from './components/Authenticator';

export default class App extends Component {
    static displayName = App.name;

    constructor(props) {
        super(props);
        loadMessages(deMessages);
        loadMessages(ruMessages);
        locale(navigator.language);

        this.state = {
            token: null
        }
    }

    resetToken() {
        localStorage.setItem("token", null);
        this.setState({ token: null });
    }

    setToken(token) {
        localStorage.setItem("token", token)
        this.setState({ token: token });
    }

    getToken() {
        return localStorage.getItem("token")
    }

    render() {
        let { token } = this.state;
        if (!token) {
            this.state.token = this.getToken();
        }
        token = this.state.token;
        const isAuth = token && token.length > 0 && !isExpired(token);
       
        return (
            <div className="app">
                {Boolean(isAuth)
                    ? <Authenticator token={this.state.token} resetToken={this.resetToken}>
                        <MainFiltration resetToken={this.resetToken} />
                    </Authenticator>
                    : <AuthenticateForm setToken={(e) => this.setToken(e)} />}
            </div>
        );
    }
}