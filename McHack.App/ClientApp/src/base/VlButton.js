﻿import React, { Component } from 'react';
import './VlButton.css';

export default class VlButton extends Component {
    render() {
        const { text, onClick, width = '150px', height = '40px' } = this.props;

        return (
            <div
                className="vl-button-container"
                style={{ width: width, height: height }}
                onClick={(e) => onClick(e)}>
                <span>{text}</span>
            </div>
        );
    }
}