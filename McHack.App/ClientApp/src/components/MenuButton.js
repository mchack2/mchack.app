﻿import React, { Component, Fragment } from 'react';
import './MenuButton.css';

import { SmileTwoTone } from '@ant-design/icons';

export class MenuButton extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        const { isSelected, onClick, text } = this.props; 

        return (
            <div className="button-container" onClick={(e) => { onClick(e) }}>
                <span className="button-text">{text}</span>
            </div>
        );
    }
}