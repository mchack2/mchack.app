﻿import { Card } from 'antd';
import React, { Component } from 'react';
import ScrollView from 'devextreme-react/scroll-view';



const { Meta } = Card;
export class ProductCard extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        const { product } = this.props;
        if (!product) {
            return null;
        }
        const { name, description, manufacturerName, imageUrl } = product;

        return (
            <ScrollView>
                <div className="product-card-container" >
                    <Card                        
                        hoverable
                        style={{
                            width: '100%'
                        }}
                        cover={<img alt="example" src={imageUrl} />}
                    >
                        <Meta title={name} description={manufacturerName} />
                        <br/>
                        <div style={{  color: 'gray' }}>
                            {description}
                        </div>
                    </Card>
                </div>
            </ScrollView>
        );
    }
}