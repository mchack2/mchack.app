﻿import React, { Component } from 'react';
import { isExpired, decodeToken } from "react-jwt";
import { sendGet, sendPost } from '../api/Api';
import VlButton from '../base/VlButton';
import { Input } from 'antd';
import { StarTwoTone, LockTwoTone } from '@ant-design/icons';

import './AuthenticateForm.css';

export default class AuthenticateForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: ""
        }
    }

    signIn() {
        console.log(this.state);

        const curToken = localStorage.getItem("token");
        const notValidToken = !curToken || isExpired(curToken);
        
        if (Boolean(notValidToken)) {
            sendPost('api/auth/authenticate', { Username: this.state.username, Password: this.state.password })
                .then(auth => {
                    this.props.setToken(auth.token);
                });
        }
        else {
            this.props.setToken(curToken);
        }
    }

    signUp() {
        sendGet('users?id=1').then(data => {
            console.log(data);
            console.log(isExpired(localStorage.getItem("token")));
        });
    }

    render() {
        const jwt = localStorage.getItem('token');

        return (
            <div className="auth-form-container">
                <div className="auth-form-header">
                    <span>Авторизация</span>
                </div>
                <div className="auth-form-body">
                    <div className="auth-form-body-input">
                        <div className="auth-from-body-input-box">
                            <Input
                                placeholder="Логин"
                                prefix={<StarTwoTone twoToneColor="#52c41a" />}
                                value={this.state.username}
                                onChange={(e) => { this.setState({ username: e.target.value }) }} />
                        </div>
                        <div className="auth-from-body-input-box">
                            <Input.Password
                                placeholder="Пароль"
                                prefix={<LockTwoTone twoToneColor="#52c41a" />}
                                value={this.state.password}
                                onChange={(e) => { this.setState({ password: e.target.value }) }} />
                        </div>
                    </div>
                    <div className="auth-form-body-buttons">
                        <VlButton text="Войти" onClick={() => this.signIn()} width={'100%'}/>
                    </div>
                </div>
            </div>
        );
    }
}