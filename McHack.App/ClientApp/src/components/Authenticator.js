﻿import React, { Component } from 'react';
import { isExpired } from "react-jwt";

import './MainFiltration.css';

export default class Authenticator extends Component {
    constructor(props) {
        super(props);        
    }

    render() {
        const { token, resetToken } = this.props;
        if (token && !isExpired(token)) {
            return <div style={{ width: '100%', height: '100%' }}>{this.props.children}</div>;
        }
        else {
            resetToken();
            return null;
        }
    }
}