﻿import React, { Component, Fragment } from 'react';
import './MainFiltration.css';
import { Cards } from './Cards';
import { MenuButton } from './MenuButton';
import DataGrid, {
    Column,
    Sorting,
    FilterRow,
    Selection,
    HeaderFilter,
    FilterPanel,
    FilterBuilderPopup,
    Scrolling,
    SearchPanel
} from 'devextreme-react/data-grid';
import { Popup, Position, ToolbarItem } from 'devextreme-react/popup';
import Search from 'antd/lib/input/Search';

import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';
import { ProductCard } from './ProductCard';
import VlButton from '../base/VlButton';
import { RequestForm } from './RequestForm';

export class MainFiltration extends Component {
    constructor(props) {
        super(props);

        this.state = {
            manufactureres: [],
            dataLoading: true,
            showCardPopup: false,
            selectedProduct: null,

            showRequestPopup: false
        }
    }

    componentDidMount() {
        fetch('data/test2')
            .then(response => response.json())
            .then(data => {
                console.log(data);

                this.setState({
                    manufactureres: data,
                    dataLoading: false
                })
            })
            .catch(err => {
                console.error(err);
                this.setState({
                    dataLoading: false
                })
            });
    }

    onToolbarPreparing(e) {
        console.log(e);
        e.toolbarOptions.items[0].location = 'before';

    }

    onRowDblClick = (options) => {
        console.log(options);

        const data = options.data;

        this.setState({
            selectedProduct: data,
            showCardPopup: true
        });
    }

    sendRequest = (e) => {
        this.setState({
            showRequestPopup: true
        })
    }

    exit = (e) => {
        this.props.resetToken();
        window.location.reload();
    }

    render() {
        const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
        const product = this.state.selectedProduct;

        return (
            <div className="main-container">
                <Popup
                    width='30%'
                    height='75%'
                    title={product && product.name}
                    dragEnabled={false}
                    closeOnOutsideClick={true}
                    visible={this.state.showCardPopup}
                    defaultVisible={false}
                    onHiding={(e) => { this.setState({ showCardPopup: false }) }}
                >
                    <div style={{
                        width: '100%',
                        height: '100%',
                        display: 'flex',
                        flexDirection: 'column',
                    }}>
                        <ProductCard product={this.state.selectedProduct} />
                        <br />
                        <div>
                            <VlButton text="Перейти к источнику" width='100%' onClick={(e) => {
                                var win = window.open(product.url, '_blank');
                                win.focus();
                            }} />
                        </div>
                    </div>
                </Popup>
                <Popup
                    visible={this.state.showRequestPopup}
                    width={550}
                    height={530}
                    title="Запрос на добавление"
                    onHiding={(e) => { this.setState({ showRequestPopup: false }) }}
                >
                    <RequestForm />
                </Popup>
                <div className="filtration-header">
                    <div className="filtration-container">
                    </div>
                    <div className="filtration-container">
                        <MenuButton text="Подать заявку" onClick={(e) => { this.sendRequest(e); }} />
                        <MenuButton text="Статистика" onClick={(e) => { this.statistic(e); }} />
                        <MenuButton text="Выйти" onClick={(e) => { this.exit(e); }} />
                    </div>
                </div>
                <div className="main-center">
                    {
                        Boolean(this.state.dataLoading) ?
                            <Spin indicator={antIcon} size={'large'} /> :
                            <DataGrid
                                dataSource={this.state.manufactureres}
                                showBorders={true}
                                onToolbarPreparing={this.onToolbarPreparing}
                                ref={(ref) => { this.dataGrid = ref; }}
                                height='100%'
                                onRowDblClick={this.onRowDblClick}
                            >
                                <Sorting mode="multiple" />
                                <SearchPanel visible={true} width='100%' />
                                <Selection mode='single' />
                                <HeaderFilter
                                    allowSearch={true}
                                    showRelevantValues={true}
                                    width={300}
                                    height={400} />
                                <FilterRow visible={true} />
                                <FilterPanel visible={true} />
                                <Column
                                    dataField="name"
                                    caption="Наименование"
                                />
                                <Column
                                    dataField="manufacturerName"
                                    caption="Производитель"
                                    defaultSortOrder="asc" />
                                <Column
                                    dataField="description"
                                    caption="Описание"
                                    defaultSortOrder="asc" />
                                <Column
                                    dataField="categories[0].name"
                                    caption="Категория"
                                    defaultSortOrder="asc">
                                    <HeaderFilter visible={false} />
                                </Column>
                                <Column
                                    dataField="price"
                                    caption="Цена"
                                    defaultSortOrder="asc">
                                    <HeaderFilter visible={false} />
                                </Column>
                            </DataGrid>
                    }
                </div>
            </div>
        );
    }
}