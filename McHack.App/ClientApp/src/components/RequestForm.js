﻿import { Card } from 'antd';
import React, { Component } from 'react';
import ScrollView from 'devextreme-react/scroll-view';
import {
    Radio
} from 'antd';
import notify from 'devextreme/ui/notify';
import Form, {
    SimpleItem,
    GroupItem,
    Label,
} from 'devextreme-react/form';

import 'devextreme-react/text-area';

import VlButton from '../base/VlButton';
import { RequestProductForm } from './RequestProductForm';

export class RequestForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isManufacturer: true,
            categories: [],
            manufacturer: {
                name: "",
                description: "",
                category: "",
                url: "",
                address: "",
                phone: "",
                email: "",
                webSite: "",
                officialName: "",
                ogrn: null,
                inn: null,
                kpp: null,
                officialAddress: ""
            },
            products:[]
        }
    }

    onRequestTargetChanged = (e) => {
        this.setState({
            isManufacturer: e.target.value === "a"
        });
    }

    componentDidMount = () => {
        fetch('data/test3')
            .then(response => response.json())
            .then(data => {
                const { manufacturer } = this.state;
                this.setState({
                    categories: data
                });
            });
    }

    render() {
        return (
            <div style={{ display: 'flex', flexDirection: 'column' }}>
                <Radio.Group defaultValue="a" size="large">
                    <Radio.Button value="a" onChange={(e) => { this.onRequestTargetChanged(e) }}>Предприятия</Radio.Button>
                    <Radio.Button value="b" onChange={(e) => { this.onRequestTargetChanged(e) }}>Продуктов/услуг</Radio.Button>
                </Radio.Group>
                {
                    Boolean(this.state.isManufacturer)
                        ? <div style={{ display: 'flex', flexDirection: 'column', height: '396px' }}>
                            <ScrollView style={{ marginTop: '15px', marginBottom: '15px' }}>
                                <Form formData={this.state.manufacturer} >
                                    <GroupItem cssClass="first-group">
                                        <GroupItem>
                                            <SimpleItem dataField="name">
                                                <Label text="Наименование" />
                                            </SimpleItem>
                                            <SimpleItem
                                                dataField="category"
                                                editorType="dxSelectBox"
                                                editorOptions={{ items: this.state.categories.map(x => x.name) }}
                                            >
                                                <Label text="Категория" />
                                            </SimpleItem>
                                            <SimpleItem dataField="address" >
                                                <Label text="Адрес" />
                                            </SimpleItem>
                                            <SimpleItem dataField="phone" >
                                                <Label text="Телефон" />
                                            </SimpleItem>
                                            <SimpleItem dataField="email" >
                                                <Label text="EMail" />
                                            </SimpleItem>
                                            <SimpleItem dataField="webSite" >
                                                <Label text="ВЭБ-сайт" />
                                            </SimpleItem>
                                            <SimpleItem dataField="ogrn" >
                                                <Label text="ОГРН" />
                                            </SimpleItem>
                                            <SimpleItem dataField="inn" >
                                                <Label text="ИНН" />
                                            </SimpleItem>
                                            <SimpleItem dataField="kpp" >
                                                <Label text="КПП" />
                                            </SimpleItem>
                                            <SimpleItem dataField="officialAddress" >
                                                <Label text="ЮР. Адрес" />
                                            </SimpleItem>
                                        </GroupItem>
                                    </GroupItem>
                                </Form>

                            </ScrollView>
                            <VlButton text="Отправить" width='100%' height='12%' onClick={(e) => {
                                const data = {
                                    ...this.state.manufacturer,
                                    category: this.state.categories.find(x => x.name === this.state.manufacturer.category)
                                }
                                fetch('data/AddNewRequestManufacturer', { method: 'POST', body: JSON.stringify({manufacturer:data}) })
                                    .then(response => {
                                        const a = response;
                                        notify('Отправлено!', 'success', 3000);
                                    })
                                    .catch(err => {

                                        notify('Отправлено!', 'error', 3000);
                                    })
                            }} />
                        </div>
                        : <div>
                            <RequestProductForm />
                        </div>
                }

            </div>
        );
    }
}


/* 
 * 
 * <Form formData={this.state.manufacturer} >
                                    <GroupItem cssClass="first-group">
                                        <GroupItem>
                                            <SimpleItem dataField="name">
                                                <Label text="Наименование" />
                                            </SimpleItem>
                                            <SimpleItem
                                                dataField="category"
                                                editorType="dxSelectBox"
                                                editorOptions={this.stateOptions}>
                                                <Label text="Категория" />
                                            </SimpleItem>
                                            <SimpleItem dataField="address" >
                                                <Label text="Адрес" />
                                            </SimpleItem>
                                            <SimpleItem dataField="phone" >
                                                <Label text="Телефон" />
                                            </SimpleItem>
                                            <SimpleItem dataField="email" >
                                                <Label text="EMail" />
                                            </SimpleItem>
                                            <SimpleItem dataField="webSite" >
                                                <Label text="ВЭБ-сайт" />
                                            </SimpleItem>
                                            <SimpleItem dataField="ogrn" >
                                                <Label text="ОГРН" />
                                            </SimpleItem>
                                            <SimpleItem dataField="inn" >
                                                <Label text="ИНН" />
                                            </SimpleItem>
                                            <SimpleItem dataField="kpp" >
                                                <Label text="КПП" />
                                            </SimpleItem>
                                            <SimpleItem dataField="officialAddress" >
                                                <Label text="ЮР. Адрес" />
                                            </SimpleItem>
                                        </GroupItem>
                                    </GroupItem>
                                </Form>
 * 
 * 
 * */ 