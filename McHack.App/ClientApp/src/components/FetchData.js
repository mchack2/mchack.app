import React, { Component, Fragment } from 'react';

import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';

export class FetchData extends Component {
    static displayName = FetchData.name;

    constructor(props) {
        super(props);

        this.state = {
            data: null,
            isLoading: true
        };
    }



    componentDidMount() {
        this.populateWeatherData();
    }

    render() {
        const { isLoading } = this.state;
        const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

        return (
            <div>
                {
                    Boolean(isLoading) ?
                        <Spin indicator={antIcon} /> :
                        <div>
                            {this.state.data} - продуктов в БД
                        </div>
                }
            </div>
        );
    }

    async populateWeatherData() {
        fetch('test/test')
            .then(response => response.text())
            .then(result => {
                this.setState({
                    data: result,
                    isLoading: false
                })
            })
    }
}
