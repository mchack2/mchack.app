﻿import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Form, Input, Space } from 'antd';
import React, { Component } from 'react';
import ScrollView from 'devextreme-react/scroll-view';
import VlButton from '../base/VlButton';


export class RequestProductForm extends Component {
    constructor(props) {
        super(props);

        this.state = {

        }
    }

    onFinish = (e) => {
        console.log(e);
    }

    render() {
        return (
            <div style={{ margin: '10px' }}>
                <ScrollView style={{ marginTop: '15px', marginBottom: '15px' }}>
                    <Form name="dynamic_form_nest_item" onFinish={this.onFinish} autoComplete="off">
                        <Form.List name="users">
                            {(fields, { add, remove }) => (
                                <>
                                    {fields.map(({ key, name, ...restField }) => (
                                        <Space key={key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                                            <Form.Item
                                                {...restField}
                                                name={[name, 'name']}
                                            >
                                                <Input placeholder="Наименование" />
                                            </Form.Item>
                                            <Form.Item
                                                {...restField}
                                                name={[name, 'image']}
                                            >
                                                <Input placeholder="Изображение" />
                                            </Form.Item>
                                            <Form.Item
                                                {...restField}
                                                name={[name, 'description']}
                                            >
                                                <Input placeholder="Описание" />
                                            </Form.Item>
                                            <MinusCircleOutlined onClick={() => remove(name)} />
                                        </Space>
                                    ))}
                                    <Form.Item>
                                        <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                            Добавить
                                        </Button>
                                    </Form.Item>
                                </>
                            )}
                        </Form.List>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" color="#299f55">
                                Отправить
                            </Button>
                        </Form.Item>
                    </Form>
                </ScrollView>
            </div>
        );
    }
}