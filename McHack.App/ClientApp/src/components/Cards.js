﻿import React, { Component } from 'react';
import { ProductCard } from './ProductCard';
import './Cards.css';

export class Cards extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cards: 				[
					{
						"Name": "Насосные установки гидравлические",
						"Manufacturer": "Производитель гидравлического оборудования «Гидросфера»",
						"ImageUrl": "https:\/\/productcenter.ru\/images\/595807-nasosnyie-ustanovki-gidravlichieskiie-540x480.jpg"
					},
					{
						"Name": "Инжектор масла",
						"Manufacturer": "Производитель гидравлического оборудования «Гидросфера»",
						"ImageUrl": "https:\/\/productcenter.ru\/images\/601498-inzhiektor-masla-540x480.png"
					},
					{
						"Name": "Инструмент для станков - качалок",
						"Manufacturer": "Производитель гидравлического оборудования «Гидросфера»",
						"ImageUrl": "https:\/\/productcenter.ru\/images\/595763-instrumient-dlia-stankov-kachalok-540x480.jpg"
					},
					{
						"Name": "Установка перемещения тяжеловесного оборудования",
						"Manufacturer": "Производитель гидравлического оборудования «Гидросфера»",
						"ImageUrl": "https:\/\/productcenter.ru\/images\/595759-ustanovka-pieriemieshchieniia-tiazhieloviesnogo-oborudovaniia-540x480.jpg"
					},
					{
						"Name": "Гайкорез гидравлический",
						"Manufacturer": "Производитель гидравлического оборудования «Гидросфера»",
						"ImageUrl": "https:\/\/productcenter.ru\/images\/595754-gaikoriez-gidravlichieskii-540x480.jpg"
					},
					{
						"Name": "Насосы гидравлические с ручным и ножным приводом",
						"Manufacturer": "Производитель гидравлического оборудования «Гидросфера»",
						"ImageUrl": "https:\/\/productcenter.ru\/images\/595740-nasosy-gidravlichieskiie-s-ruchnym-i-nozhnym-privodom-540x480.jpg"
					},
					{
						"Name": "Домкраты гидравлические",
						"Manufacturer": "Производитель гидравлического оборудования «Гидросфера»",
						"ImageUrl": "https:\/\/productcenter.ru\/images\/595719-domkraty-nizkiie-540x480.jpg"
					},
					{
						"Name": "Гольфы по индивидуальному дизайну",
						"Manufacturer": "Производитель носков «ERA Teks»",
						"ImageUrl": "https:\/\/productcenter.ru\/images\/492445-golfy-po-individualnomu-dizainu-era-teks-540x480.jpg"
					},
					{
						"Name": "Гетры с индивидуальным дизайном",
						"Manufacturer": "Производитель носков «ERA Teks»",
						"ImageUrl": "https:\/\/productcenter.ru\/images\/534499-gietry-s-individualnym-dizainom-540x480.png"
					},
					{
						"Name": "Дизайнерские носки с принтом",
						"Manufacturer": "Производитель носков «ERA Teks»",
						"ImageUrl": "https:\/\/productcenter.ru\/images\/492430-noski-po-individualnomu-dizainu-era-teks-540x480.jpg"
					}
				]
        }
    }



    render() {
		return (
			<div className="cards-container" >
				{this.state.cards.map(x => {
					return (
						<ProductCard name={x.Name} manufacturer={x.Manufacturer} imgUrl={x.ImageUrl} />
                    )
                })}
            </div>
        );
    }
}