﻿var options = {
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
        'Content-Type': 'application/json'
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *client
}

export const sendPost = (url, body) => {
    console.log(url);

    return fetch(url, {
        ...options,        
        method: 'POST',
        headers: {
            ...options.headers,
            Authorization: `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify(body)
    })
        .then(response => response.json())
        .then(data => {
            return data;
        })
        .catch(err => {
            console.error(err);
        });
}

export const sendGet = (url) => {
    return fetch(url, {
        ...options,
        method: 'GET',
        headers: {
            ...options.headers,
            Authorization: `Bearer ${localStorage.getItem("token") }`
        },
    })
        .then(response => response.json())
        .then(data => {
            return data;
        })
        .catch(err => {
            console.error(err);
        });
}