﻿using McHack.App.Model;
using McHack.App.Model.ViewData;
using McHack.Data.Data;
using McHack.Data.Repository;
using McHack.Domain;
using McHack.Service;
using McHack.Service.Dto;
using McHack.Service.Serialize;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace McHack.App.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DataController : ControllerBase
    {
        private readonly ILogger<DataController> _logger;
        private readonly ProductRepository _productRepository;
        private readonly ProductService _productService;
        private readonly ProductCategoryService _productCategoryService;
        private readonly ManufacturerService _manufacturerService;
        private readonly RequestService _requestService;

        public DataController(ILogger<DataController> logger, IDbContextFactory<ManufacturerContext> contextFactory)
        {
            _logger = logger;

            var context = contextFactory.CreateDbContext();
            _productService = new ProductService(context);
            _productCategoryService = new ProductCategoryService(context);
            _productRepository = new ProductRepository(context);
            _manufacturerService = new ManufacturerService(context);
            _requestService = new RequestService(context);
        }


        [HttpGet("test")]
        public IEnumerable<Product> Get()
        {
            var aa = _productRepository.GetWithCategories().ToList();
            return aa;
        }

        [HttpGet("test2")]
        public IEnumerable<ProductViewData> GetProductViewData()
        {
            return _productService.GetAllProducts().Select(p => new ProductViewData(p)).ToArray();
        }

        [HttpGet("test3")]
        public IEnumerable<CategoryShortViewData> GetCategoryShortData()
        {
            return _productCategoryService.GetAllShortCategories().Select(p => new CategoryShortViewData(p.Id, p.Name)).ToArray();
            //return _productCategoryService.GetAllShortCategories().Select(p => new CategoryShortViewData(p.Id, p.Name)).ToArray();
        }

        [HttpGet("GetAnalogProducts")]
        public IEnumerable<ProductShortViewData> GetAnalogProducts(int productId)
        {
            return _productService.GetAnalogProducts(productId).Select(p => new ProductShortViewData(p)).ToArray();
        }
        [HttpGet("GetManufacturerAnalogProducts")]
        public IEnumerable<ManufacturerShortViewData> GetManufacturerAnalogProducts(int productId)
        {
            return _manufacturerService.GetManufacturerAnalogProducts(productId).Select(p => new ManufacturerShortViewData(p.Id, p.Name, p.Description)).ToArray();
        }


        [HttpPost("AddNewRequestProduct")]
        public void AddRequestProduct(RequestProductsInputModel model)
        {
            var serializeService = new ProductsSerialize();
            var productsDto = model.ProductInputs.Select(p => new ProductShortDto()
            {
                Id = p.Id,
                Name = p.Name,
                ImageUrl = p.ImageUrl,
                Price = p.Price
            });

            var jsonObj = serializeService.Serialize(productsDto);

            var requestDto = new RequestDto()
            {
                User = new UserDto() { Id= model.UserId},
                RequestKind = Util.Enum.RequestType.ProductAdd,
                Status = Util.Enum.StatusType.Open,
                SerializationText = jsonObj
            };

            _requestService.AddRequest(requestDto);
        }

        [HttpPost("AddNewRequestManufacturer")]
        public IActionResult AddRequestManufacturer(RequestManufacturerInputModel model)
        {
            try
            {
                var serializeService = new ManufacturerSerialize();
                var manufacturerDto = new ManufacturerDto()
                {
                    Name = model.Manufacturer.Name,
                    Description = model.Manufacturer.Description,
                    Category = model.Manufacturer.Category,
                    Url = model.Manufacturer.Url,
                    Address = model.Manufacturer.Address,
                    Phone = model.Manufacturer.Phone,
                    Email = model.Manufacturer.Email,
                    WebSite = model.Manufacturer.WebSite,
                    OfficialName = model.Manufacturer.OfficialName,
                    OGRN = model.Manufacturer.OGRN,
                    INN = model.Manufacturer.INN,
                    KPP = model.Manufacturer.KPP,
                    OfficialAddress = model.Manufacturer.OfficialAddress,
                    Owner = new UserDto() { Id = model.UserId }
                };

                var jsonObj = serializeService.Serialize(manufacturerDto);

                var requestDto = new RequestDto()
                {
                    User = new UserDto() { Id = model.UserId },
                    RequestKind = Util.Enum.RequestType.VerificationUser,
                    Status = Util.Enum.StatusType.Open,
                    SerializationText = jsonObj
                };

                _requestService.AddRequest(requestDto);

                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet("GetAllRequests")]
        public IEnumerable<RequestViewData> GetRequests(int userId = 0)
        {
            return _requestService.GetAllRequests(userId).Select(x => new RequestViewData(x.Id, x.User.Name, x.RequestKind, x.Status, x.SerializationText)).ToArray();
        }

        [HttpGet("DeserializeProduct")]
        public IEnumerable<ProductShortViewData> GetRequestProducts(string serializionText)
        {
            var serializeService = new ProductsSerialize();
            return  serializeService
                .Deserialize(serializionText)
                .Select(x=> new ProductShortViewData(x.Name, x.ImageUrl, x.Price ))
                .ToArray();
        }

        [HttpGet("DeserializeManufacturer")]
        public ManufacturerForRequestViewData GetRequestManufacturer(string serializionText)
        {
            var serializeService = new ManufacturerSerialize();
            var dto = serializeService.Deserialize(serializionText);
            return new ManufacturerForRequestViewData(dto);
        }

        //public void UpdateFullRequest(RequestInputModel requestInput)
        //{
        //    string serializionText;

        //    if (requestInput is ManufacturerForRequestViewData)

        //}
    }
}