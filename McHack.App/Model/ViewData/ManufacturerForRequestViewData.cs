﻿using McHack.Service.Dto;

namespace McHack.App.Model.ViewData
{
    public class ManufacturerForRequestViewData
    {
        public ManufacturerForRequestViewData(ManufacturerDto dto)
        {
            Name = dto.Name;
            Description = dto.Description;
            Category = dto.Category;
            Url = dto.Url;
            Address = dto.Address;
            Phone = dto.Phone;
            Email = dto.Email;
            WebSite = dto.WebSite;
            OfficialName = dto.OfficialName;
            OGRN = dto.OGRN;
            INN = dto.INN;
            KPP = dto.KPP;
            OfficialAddress = dto.OfficialAddress;
        }


        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? Category { get; set; }
        public string? Url { get; set; }
        public string? Address { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
        public string? WebSite { get; set; }
        public string? OfficialName { get; set; }
        public string? OGRN { get; set; }
        public string? INN { get; set; }
        public string? KPP { get; set; }
        public string? OfficialAddress { get; set; }
    }
}
