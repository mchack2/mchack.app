﻿using McHack.Service.Dto;

namespace McHack.App.Model.ViewData
{
    public class ProductViewData
    {
        public ProductViewData(ProductDto dto)
        {
            Id = dto.Id;
            Name = dto.Name;
            Url = dto.Url;
            Description = dto.Description;
            ImageUrl = dto.ImageUrl;
            Price = dto.Price;
            ManufacturerName = dto.Manufacturer?.Name;
            Categories = GetCategory(dto.Category);
        }

        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Url { get; set; }
        public string? Description { get; set; }
        public string? ImageUrl { get; set; }
        public string? Price { get; set; }
        public string ManufacturerName { get; set; }
        public IEnumerable<ProductCategoryViewData> Categories { get; set; }

        private IEnumerable<ProductCategoryViewData> GetCategory(ProductCategoryDto categoryDto)
        {
            if (categoryDto == null)
                return null; 

            int i = 1;
            var categoriesArray = new List<ProductCategoryViewData>();
            SetCategory(categoriesArray, categoryDto, i);

            return categoriesArray;
        }

        private void SetCategory(ICollection<ProductCategoryViewData>  categoriesArray, ProductCategoryDto categoryDto, int i)
        {
            categoriesArray.Add(new ProductCategoryViewData(i++, categoryDto.Name));

            if (categoryDto.Parent != null)
                SetCategory(categoriesArray, categoryDto.Parent, i);
        }
    }
}
