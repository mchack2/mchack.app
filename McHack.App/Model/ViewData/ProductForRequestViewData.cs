﻿namespace McHack.App.Model.ViewData
{
    public class ProductForRequestViewData
    {
        public string? Name { get; set; }
        public string? Url { get; set; }
        public string? Description { get; set; }
        public string? ImageUrl { get; set; }
        public string? Price { get; set; }
        public int? CategoryId { get; set; }
    }
}
