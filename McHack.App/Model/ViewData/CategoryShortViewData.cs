﻿namespace McHack.App.Model.ViewData
{
    public class CategoryShortViewData
    {
        public CategoryShortViewData(int id, string name)
        {
            Id = id;
            Name = name;
        }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
