﻿namespace McHack.App.Model.ViewData
{
    public class ProductCategoryViewData
    {
        public ProductCategoryViewData(int level, string name)
        {
            Level = level;
            Name = name;
        }
        public int Level { get;set; }
        public string Name { get;set; }
    }
}
