﻿using McHack.Util.Enum;

namespace McHack.App.Model.ViewData
{
    public class RequestViewData
    {
        public RequestViewData(int id, string userName, RequestType requestKind, StatusType status, string serializationText)
        {
            Id = id;
            UserName = userName;
            RequestKind = requestKind;
            Status = status;
            SerializationText = serializationText;
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public RequestType RequestKind { get; set; }
        public StatusType Status { get; set; }
        public string SerializationText { get; set; }
    }
}
