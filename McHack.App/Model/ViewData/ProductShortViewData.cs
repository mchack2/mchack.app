﻿using McHack.Service.Dto;

namespace McHack.App.Model.ViewData
{
    public class ProductShortViewData
    {
        public ProductShortViewData(string name, string imageUrl, string price)
        {
            Name = name;
            ImageUrl = imageUrl;
            Price = price;
        }
        public ProductShortViewData(ProductShortDto dto)
        {
            Id = dto.Id;
            Name = dto.Name;
            ImageUrl = dto.ImageUrl;
            Price = dto.Price;
        }

        public int Id { get; set; }
        public string? Name { get; set; }
        public string? ImageUrl { get; set; }
        public string? Price { get; set; }
    }
}
