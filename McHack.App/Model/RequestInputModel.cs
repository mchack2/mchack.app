﻿namespace McHack.App.Model
{
    public class RequestInputModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
    }
}
