﻿using McHack.App.Model.ViewData;

namespace McHack.App.Model
{
    public class RequestManufacturerInputModel : RequestInputModel
    {  
        //public int UserId { get; set; }
        public ManufacturerForRequestViewData Manufacturer { get; set; }
    }
}
