﻿using McHack.App.Model.ViewData;

namespace McHack.App.Model
{
    public class RequestProductsInputModel : RequestInputModel
    {
        public IEnumerable<ProductShortViewData> ProductInputs { get; set; }
    }
}
