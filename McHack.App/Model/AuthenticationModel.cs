﻿using System.ComponentModel.DataAnnotations;

namespace McHack.App.Model
{
    public class AuthenticationModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
