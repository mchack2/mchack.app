﻿using McHack.Domain.DBDictionary;

namespace McHack.Domain
{
    public class Request
    {
        public int Id { get; set; }
        public User User { get; set; }
        public RequestKind RequestKind { get; set; }
        public Status Status { get; set; }
        public string SerializationText { get; set; }
    }
}
