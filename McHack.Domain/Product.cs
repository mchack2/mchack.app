﻿namespace McHack.Domain
{
    public class Product
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Url { get; set; }
        public string? Description { get; set; }
        public string? ImageUrl { get; set; }
        public string? Price { get; set; }
        public virtual ProductCategory Category { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
    }
}