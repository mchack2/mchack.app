﻿namespace McHack.Domain
{
    public class WebSiteSetting
    {
        public int Id { get; set; } 
        public string RootUrl { get; set; }
        public string? CatalogPart { get; set; }
        public string? ExtendPart { get; set; }
    }
}
