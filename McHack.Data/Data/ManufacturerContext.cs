﻿using McHack.Domain;
using McHack.Domain.DBDictionary;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McHack.Data.Data
{
    public class ManufacturerContext : DbContext
    {
        public ManufacturerContext(DbContextOptions<ManufacturerContext> options)
            : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasPostgresExtension("postgis");

            //builder.Entity<Testtable>()
        }

        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }

        public DbSet<RequestKind> RequestKinds { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<WebSiteSetting> WebSiteSettings { get; set; }
    }
}
