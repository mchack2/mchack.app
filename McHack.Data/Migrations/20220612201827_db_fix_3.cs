﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace McHack.Data.Migrations
{
    public partial class db_fix_3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Merchandisies");

            migrationBuilder.DropTable(
                name: "Companies");

            migrationBuilder.AddColumn<int>(
                name: "OwnerId",
                table: "Manufacturers",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Manufacturers_OwnerId",
                table: "Manufacturers",
                column: "OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Manufacturers_Users_OwnerId",
                table: "Manufacturers",
                column: "OwnerId",
                principalTable: "Users",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manufacturers_Users_OwnerId",
                table: "Manufacturers");

            migrationBuilder.DropIndex(
                name: "IX_Manufacturers_OwnerId",
                table: "Manufacturers");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "Manufacturers");

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Description = table.Column<string>(type: "text", nullable: true),
                    INN = table.Column<int>(type: "integer", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: false),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    Url = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Merchandisies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompanyId = table.Column<int>(type: "integer", nullable: true),
                    ProductCategoryId = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Url = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandisies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Merchandisies_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Merchandisies_ProductCategories_ProductCategoryId",
                        column: x => x.ProductCategoryId,
                        principalTable: "ProductCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Merchandisies_CompanyId",
                table: "Merchandisies",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandisies_ProductCategoryId",
                table: "Merchandisies",
                column: "ProductCategoryId");
        }
    }
}
