﻿using McHack.Domain;
using Microsoft.EntityFrameworkCore;

namespace McHack.Data.Repository
{
    public class UserRepository : Repository<User>
    {
        public UserRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<User> GetAll()
        {
            return _dbSet.Include(x=>x.Role);
        }

        public User GetByUsernameAndPassword(string username, string password)
        {
            return _dbSet.Include(X=>X.Role).SingleOrDefault(x => x.Name == username && x.Password == password);
        }
    }
}
