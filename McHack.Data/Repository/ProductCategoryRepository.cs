﻿using McHack.Domain;
using Microsoft.EntityFrameworkCore;

namespace McHack.Data.Repository
{
    public class ProductCategoryRepository : Repository<ProductCategory>
    {
        public ProductCategoryRepository(DbContext context) : base(context){ }

        public IEnumerable<ProductCategory> GetСhildCategories(int categoryId)
        {
            return _dbSet.Where(c => c.Parent != null &&  c.Parent.Id == categoryId);
        }

        //public IEnumerable<ProductCategory> GetAllCategories()
        //{
        //    return _dbSet.ToArray();
        //}
    }
}
