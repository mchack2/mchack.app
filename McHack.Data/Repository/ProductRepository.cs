﻿using McHack.Domain;
using Microsoft.EntityFrameworkCore;

namespace McHack.Data.Repository
{
    public class ProductRepository : Repository<Product>
    {
        public ProductRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<Product> GetWithCategories()
        {
            return _dbSet
                .Include(x=>x.Manufacturer)
                .Include(x => x.Category)
                .ThenInclude(x => x.Parent).ToList();
        }

        public IEnumerable<Product> GetByCategoryIds(IEnumerable<int> ids)
        {
            return _dbSet
                .Include(x=> x.Category)
                .Include(x => x.Manufacturer)
                .Where(x => ids.Contains(x.Category.Id));
        }
        public Product GetById(int id)
        {
            return _dbSet
                .Include(x => x.Category)
                .Include(x => x.Manufacturer)
                .FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Product> GetAll()
        {
            return _dbSet
                .Include(x => x.Category)
                .Include(x => x.Manufacturer)
                .ToArray();
        }

    }
}
