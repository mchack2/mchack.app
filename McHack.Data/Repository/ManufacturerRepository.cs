﻿using McHack.Domain;
using Microsoft.EntityFrameworkCore;

namespace McHack.Data.Repository
{
    public class ManufacturerRepository : Repository<Manufacturer>
    {
        public ManufacturerRepository(DbContext context) : base(context){ }

        public IEnumerable<Manufacturer> GetWithProducts()
        {
            return _dbSet
                .Include(x=>x.Owner)
                .Include(x => x.Products);
        }
        public IEnumerable<Manufacturer> GetAll()
        {
            return _dbSet
                .Include(x=> x.Owner)
                .Include(x => x.Products)
                .ThenInclude(x => x.Category)
                .ThenInclude(x => x.Parent);
        }

        public Manufacturer GetByUserId(int id)
        {
            return _dbSet
                .Include(x => x.Owner)
                .FirstOrDefault(x => x.Owner.Id == id);
        }

    }
}
