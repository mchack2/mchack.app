﻿using McHack.Domain;
using McHack.Domain.DBDictionary;
using Microsoft.EntityFrameworkCore;

namespace McHack.Data.Repository
{
    public class DBDictionaryRepository : Repository<IDBDictonary>
    {
        public DBDictionaryRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<IDBDictonary> GetAll()
        {
            return _dbSet.ToArray();
        }
    }
}
