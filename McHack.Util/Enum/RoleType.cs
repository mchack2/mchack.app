﻿namespace McHack.Util.Enum
{
    public enum RoleType
    {
        Moderator = 1,
        Company = 2,
        Beholder = 3
    }
}
