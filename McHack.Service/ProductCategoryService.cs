﻿using McHack.Data.Data;
using McHack.Data.Repository;
using McHack.Service.Dto;
using McHack.Service.Map;

namespace McHack.Service
{
    public class ProductCategoryService
    {
        private readonly ProductCategoryRepository _productCategoryRepository;
        private readonly ProductCategoryMap _productCategoryMap;
        public ProductCategoryService(ManufacturerContext context)
        {
            _productCategoryMap = new ProductCategoryMap();

            _productCategoryRepository = new ProductCategoryRepository(context);
        }

        public IEnumerable<ProductCategoryShortDto> GetAllShortCategories()
        {
            //return _productCategoryRepository.GetAll().Select(_productCategoryMap.MapToShortDto).ToArray();
            return _productCategoryRepository.Get(x=>x.Parent is null).Select(_productCategoryMap.MapToShortDto).ToArray();
        }
    }
}
