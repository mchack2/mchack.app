﻿using McHack.Domain;
using McHack.Service.Dto;

namespace McHack.Service.Map
{
    public class ProductMap : BaseMap<Product, ProductDto>
    {
        private readonly ProductCategoryMap _productCategoryMap;
        public ProductMap()
        {
            _productCategoryMap = new ProductCategoryMap();
        }

        public IEnumerable<Product> MapToDomains(IEnumerable<ProductDto> array)
        {
            return array.Select(MapToDomain);
        }

        public Product MapToDomain(ProductDto dto)
        {
            return new Product()
            {
                Id = dto.Id,
                Name = dto.Name,
                Url = dto.Url,
                Description = dto.Description,
                ImageUrl = dto.ImageUrl,
                Price = dto.Price,
            };
        }

        public override ProductDto MapToDto(Product domain)
        {
            return new ProductDto()
            {
                Id = domain.Id,
                Name = domain.Name,
                Url = domain.Url,
                Description = domain.Description,
                ImageUrl = domain.ImageUrl,
                Price = domain.Price,
                Manufacturer = domain.Manufacturer == null ? null : new ManufacturerDto() { Id = domain.Manufacturer.Id, Name = domain.Manufacturer.Name },
                Category = domain.Category != null ? _productCategoryMap.MapToDto(domain.Category) : null
            };
        }

        public ProductShortDto MapToShortDto(Product domain)
        {
            return new ProductShortDto()
            {
                Id=domain.Id,
                Name= domain.Name,
                ImageUrl = domain.ImageUrl,
                Price = domain.Price
            };
        }
            
    }
}
