﻿namespace McHack.Service.Map
{
    public abstract class BaseMap<Domain, Dto> where Domain : class 
        where Dto : class
    {
       public virtual IEnumerable<Dto> MapToDtos(IEnumerable<Domain> array)
       {
            return array.Select(MapToDto);
       }
       public abstract Dto MapToDto(Domain domain);
    }
}
