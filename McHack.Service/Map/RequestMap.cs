﻿using McHack.Domain;
using McHack.Service.Dto;
using McHack.Util.Enum;

namespace McHack.Service.Map
{
    public class RequestMap : BaseMap<Request, RequestDto>
    {
        private readonly UserMap _userMap;
        public RequestMap()
        {
            _userMap = new UserMap();
        }

        public override RequestDto MapToDto(Request domain)
        {
            return new RequestDto()
            {
                Id = domain.Id,
                User = _userMap.MapToDto(domain.User),
                RequestKind = (RequestType)domain.RequestKind.Id,
                Status = (StatusType)domain.Status.Id,
                SerializationText = domain.SerializationText
            };
        }
    }
}
