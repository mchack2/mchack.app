﻿using McHack.Domain;
using McHack.Service.Dto;
using McHack.Util.Exstension;

namespace McHack.Service.Map
{
    public class ProductCategoryMap : BaseMap<ProductCategory, ProductCategoryDto>
    {
        public override IEnumerable<ProductCategoryDto> MapToDtos(IEnumerable<ProductCategory> array)
        {
            var productCategoryDtos = new List<ProductCategoryDto>();
            array.Where(x => x.Parent is null).ForEach(x => MapToDto(array, x, productCategoryDtos));

            return productCategoryDtos;
        }
        public override ProductCategoryDto MapToDto(ProductCategory domain)
        {
            return new ProductCategoryDto()
            {
                Id = domain.Id,
                Name = domain.Name,
                Description = domain.Description,
                Parent = domain.Parent is null ? null : MapToDto(domain.Parent)
            };
        }

        private void MapToDto(IEnumerable<ProductCategory> categories, ProductCategory productCategory, ICollection<ProductCategoryDto> dtos, ProductCategoryDto? parent = null)
        {
            var dto = new ProductCategoryDto()
            {
                Id = productCategory.Id,
                Name = productCategory.Name,
                Description = productCategory.Description,
                Parent = parent
            };

            dtos.Add(dto);
            categories.Where(x => x.Parent == productCategory).ForEach(x => MapToDto(categories, x, dtos, dto));
        }

        public ProductCategoryShortDto MapToShortDto(ProductCategory domain)
        {
            return new ProductCategoryShortDto()
            {
                Id = domain.Id,
                Name = domain.Name  
            };
        }
    }
}
