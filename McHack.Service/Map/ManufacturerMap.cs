﻿using McHack.Domain;
using McHack.Service.Dto;

namespace McHack.Service.Map
{
    public class ManufacturerMap : BaseMap<Manufacturer, ManufacturerDto>
    {
        public override ManufacturerDto MapToDto(Manufacturer domain)
        {
            return new ManufacturerDto()
            {
                Id = domain.Id,
                Name = domain.Name,
                Description = domain.Description,
                Category = domain.Category,
                Url = domain.Url,
                Address = domain.Address,
                Phone = domain.Phone,
                Email = domain.Email,
                WebSite = domain.WebSite,
                OfficialName = domain.OfficialName,
                OGRN = domain.OGRN,
                INN = domain.INN,
                KPP = domain.KPP,
                OfficialAddress = domain.OfficialAddress,
                Owner = domain.Owner is null ? null:  new UserDto() { Id = domain.Owner.Id, Name = domain.Owner.Name }   
            };
        }

        public Manufacturer MapToDomain(ManufacturerDto dto)
        {
            return new Manufacturer()
            {
                Id = dto.Id,
                Name = dto.Name,
                Description = dto.Description,
                Category = dto.Category,
                Url = dto.Url,
                Address = dto.Address,
                Phone = dto.Phone,
                Email = dto.Email,
                WebSite = dto.WebSite,
                OfficialName = dto.OfficialName,
                OGRN = dto.OGRN,
                INN = dto.INN,
                KPP = dto.KPP,
                OfficialAddress = dto.OfficialAddress
            };
        }

        public ManufacturerShortDto MapToShortDto(Manufacturer domain)
        {
            return new ManufacturerShortDto()
            {
                Id = domain.Id,
                Name = domain.Name,
                Description = domain.Description
            };
        }
    }
}