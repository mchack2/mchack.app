﻿using McHack.Domain;
using McHack.Service.Dto;
using McHack.Util.Enum;

namespace McHack.Service.Map
{
    public class UserMap : BaseMap<User, UserDto>
    {
        public override UserDto MapToDto(User domain)
        {
            return new UserDto()
            {
                Id = domain.Id,
                Name = domain.Name,
                Role = (RoleType)domain.Role.Id,
                Token = domain.Token,
            };
        }

    }
}
