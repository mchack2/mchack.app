﻿using McHack.Data.Data;
using McHack.Service.Dto;
using McHack.Service.Map;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace McHack.Service.Auth
{
    public class AuthenticationService
    {
        private readonly AppSettings _appSettings;
        private readonly UserMap _userMap;
        private readonly UserService userService;

        public AuthenticationService(IOptions<AppSettings> appSettings, ManufacturerContext context)
        {
            _appSettings = appSettings.Value;
            _userMap = new UserMap();
            userService = new UserService(context);
        }

        public UserDto Authenticate(string username, string password)
        {
            var user = userService.GetByUsernameAndPassword(username, password);

            if (user == null)
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role.Name)
                }),
                Expires = DateTime.UtcNow.AddMinutes(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            return _userMap.MapToDto(user);
        }
    }
}
