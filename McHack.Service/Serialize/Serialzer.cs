﻿namespace McHack.Service.Serialize
{
    public class Serialzer
    {
        private readonly ManufacturerSerialize _manufacturerSerialize;
        private readonly ProductsSerialize _productsSerialize;

        public Serialzer()
        {
            _manufacturerSerialize = new ManufacturerSerialize();
            _productsSerialize = new ProductsSerialize();
        }
    }
}
