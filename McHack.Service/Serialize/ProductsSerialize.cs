﻿using McHack.Service.Dto;

namespace McHack.Service.Serialize
{
    public class ProductsSerialize : BaseSerialize<IEnumerable<ProductShortDto>> { }
}
