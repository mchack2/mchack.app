﻿using Newtonsoft.Json;

namespace McHack.Service.Serialize
{
    public abstract class BaseSerialize<T>
    {
        public T Deserialize(string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }

        public string Serialize(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
