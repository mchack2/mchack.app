﻿namespace McHack.Service.Dto
{
    public class MerchandiseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ProductCategoryDto ProductCategory { get; set; }
        public string? Url { get; set; }
    }
}
