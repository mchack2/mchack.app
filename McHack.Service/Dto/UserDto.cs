﻿using McHack.Util.Enum;

namespace McHack.Service.Dto
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public RoleType Role { get; set; }
        public string Token { get; set; }
    }
}
