﻿namespace McHack.Service.Dto
{
    public class ProductShortDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? ImageUrl { get; set; }
        public string? Price { get; set; }
    }
}
