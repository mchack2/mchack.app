﻿using McHack.Util.Enum;

namespace McHack.Service.Dto
{
    public class RequestDto
    {
        public int Id { get; set; }
        public UserDto User { get; set; }
        public RequestType RequestKind { get; set; }
        public StatusType Status { get; set; }
        public string SerializationText { get; set; }
    }
}
