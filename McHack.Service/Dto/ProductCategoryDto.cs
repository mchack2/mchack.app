﻿namespace McHack.Service.Dto
{
    public class ProductCategoryDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public ProductCategoryDto? Parent { get; set; }
    }
}
