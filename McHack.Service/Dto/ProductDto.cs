﻿namespace McHack.Service.Dto
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Url { get; set; }
        public string? Description { get; set; }
        public string? ImageUrl { get; set; }
        public string? Price { get; set; }
        public ProductCategoryDto Category { get; set; }
        public ManufacturerDto Manufacturer { get; set; }
    }
}
