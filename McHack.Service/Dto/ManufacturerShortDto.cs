﻿namespace McHack.Service.Dto
{
    public class ManufacturerShortDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
    }
}
