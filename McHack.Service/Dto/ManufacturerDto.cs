﻿namespace McHack.Service.Dto
{
    public class ManufacturerDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? Category { get; set; }
        public string? Url { get; set; }
        public string? Address { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
        public string? WebSite { get; set; }
        public string? OfficialName { get; set; }
        public string? OGRN { get; set; }
        public string? INN { get; set; }
        public string? KPP { get; set; }
        public string? OfficialAddress { get; set; }
        public UserDto Owner { get; set; }
    }
}
