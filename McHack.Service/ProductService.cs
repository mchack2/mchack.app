﻿using McHack.Data.Data;
using McHack.Data.Repository;
using McHack.Service.Dto;
using McHack.Service.Map;
using McHack.Util.Exstension;

namespace McHack.Service
{
    public class ProductService
    {
        private readonly ProductRepository _productRepository;
        private readonly ProductCategoryRepository _productCategoryRepository;
        private readonly ManufacturerRepository _manufacturerRepository;

        private readonly ProductMap _productMap;
        public ProductService(ManufacturerContext context)
        {
            _productMap = new ProductMap();

            _productRepository = new ProductRepository(context);
            _productCategoryRepository = new ProductCategoryRepository(context);
            _manufacturerRepository = new ManufacturerRepository(context);
        }

        public IEnumerable<ProductDto> GetProductsByCategoryId(int categoryId)
        {
            var categoryIds = _productCategoryRepository.GetСhildCategories(categoryId).Select(x => x.Id).ToArray();

            return _productRepository.GetByCategoryIds(categoryIds).Select(_productMap.MapToDto).ToArray();
        }

        public IEnumerable<ProductDto> GetAllProducts()
        {
            return _productRepository.GetAll().Select(_productMap.MapToDto).ToArray();
        }

        public ProductDto GetById(int id)
        {
            var product = _productRepository.GetById(id);
            return _productMap.MapToDto(product);
        }

        public IEnumerable<ProductShortDto> GetAnalogProducts(int productId)
        {
            var product = _productRepository.FindById(productId);

            if (product == null)
                return null;

            return _productRepository.GetAll()
                .Where(x => x.Category.Id == product.Category?.Id && x.Id != product.Id)
                .Select(_productMap.MapToShortDto)
                .ToArray();
        }

        public void AddProducts(IEnumerable<ProductDto> dtos, int userId)
        {
            var products = _productMap.MapToDomains(dtos);
            var manufacturer = _manufacturerRepository.GetByUserId(userId);

            products.ForEach(p=> p.Manufacturer = manufacturer);

            _productRepository.AddRange(products);
        }
    }
}
