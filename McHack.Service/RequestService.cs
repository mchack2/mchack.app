﻿using McHack.Data.Data;
using McHack.Data.Repository;
using McHack.Domain;
using McHack.Domain.DBDictionary;
using McHack.Service.Dto;
using McHack.Service.Map;
using McHack.Util.Enum;

namespace McHack.Service
{
    public class RequestService
    {
        private readonly RequestRepository _requestRepository;
        private readonly UserRepository _userRepository;
        private readonly RequestMap _requestMap;
        public RequestService(ManufacturerContext context)
        {
            _requestMap = new RequestMap();

            _requestRepository = new RequestRepository(context);
            _userRepository = new UserRepository(context);
        }

        public IEnumerable<RequestDto> GetOpenRequests(int userId)
        {
            return _requestRepository.GetAll().Where(x=> (StatusType)x.Status.Id == StatusType.Open)
                .Select(_requestMap.MapToDto)
                .ToArray();
        }

        public IEnumerable<RequestDto> GetReviewedUserRequest(int userId)
        {
            return _requestRepository.GetAll().Where(x => x.User.Id == userId && ((StatusType)x.Status.Id == StatusType.Approved || (StatusType)x.Status.Id == StatusType.Denied))
                .Select(_requestMap.MapToDto)
                .ToArray();
        }

        public IEnumerable<RequestDto> GetAllRequests(int userId=0)
        {
            if (userId != 0)
                return _requestRepository.Get().Where(x => x.User.Id == userId).Select(_requestMap.MapToDto).ToArray();

            return _requestRepository.GetAll()
                .Select(_requestMap.MapToDto)
                .ToArray();
        }

        public void AddRequest(RequestDto dto)
        {
            var user = _userRepository.FindById(dto.User.Id);

            var request = new Request()
            {
                Id = dto.Id,
                User = user,
                RequestKind = new RequestKind() { Id = (int)dto.RequestKind },
                Status = new Status() { Id = (int)dto.Status },
                SerializationText = dto.SerializationText
            };

            _requestRepository.Add(request);
        }
    }
}
