﻿using McHack.Data.Data;
using McHack.Data.Repository;
using McHack.Service.Dto;
using McHack.Service.Map;

namespace McHack.Service
{
    public class ManufacturerService
    {
        private readonly ManufacturerRepository _manufacturerRepository;
        private readonly ProductRepository _productRepository;
        private readonly ManufacturerMap _manufacturerMap;
        public ManufacturerService(ManufacturerContext context)
        {
            _manufacturerMap = new ManufacturerMap();

            _manufacturerRepository = new ManufacturerRepository(context);
            _productRepository = new ProductRepository(context);
        }

        public IEnumerable<ManufacturerDto> GetAllManufacturer()
        {
            return _manufacturerRepository.GetAll().Select(_manufacturerMap.MapToDto).ToArray();
        }

        public IEnumerable<ManufacturerDto> GetCompaniesByProductCategoryIds(IEnumerable<int> categoryIds)
        {
            return _productRepository.GetByCategoryIds(categoryIds)
                .Where(x => x.Manufacturer != null && x.Category != null && categoryIds.Contains(x.Category.Id))
                .Select(x => _manufacturerMap.MapToDto(x.Manufacturer))
                .ToArray();
        }


        public IEnumerable<ManufacturerShortDto> GetManufacturerAnalogProducts(int productId)
        {
            var product = _productRepository.FindById(productId);

            if (product == null)
                return null;

            return _productRepository.GetAll()
                .Where(x => x.Category.Id == product.Category?.Id && x.Id != product.Id && x.Manufacturer != null)
                .Select(x=>x.Manufacturer)
                .Distinct()
                .Select(_manufacturerMap.MapToShortDto)
                .ToArray();
        }

        public void AddManufacture(ManufacturerDto dto, int userId)
        {
            var manufacturer = _manufacturerMap.MapToDomain(dto);
            manufacturer.Owner = new Domain.User() { Id = userId };

            _manufacturerRepository.Add(manufacturer); 
        }
    }
}
