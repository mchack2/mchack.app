﻿using McHack.Data.Data;
using McHack.Data.Repository;
using McHack.Domain;
using McHack.Service.Dto;
using McHack.Service.Map;

namespace McHack.Service.Auth
{
    public class UserService
    {
        private readonly UserMap _userMap;
        private readonly UserRepository _userRepository;      
        public UserService(ManufacturerContext context)
        {
            _userMap = new UserMap();
            _userRepository = new UserRepository(context);
        }

        public IEnumerable<UserDto> GetAll()
        {
            return _userRepository.GetAll().Select(_userMap.MapToDto).ToArray();
        }

        public UserDto GetById(int id)
        {
            var user = _userRepository.Get().FirstOrDefault(x => x.Id == id);
            return _userMap.MapToDto(user);
        }

        public User GetByUsernameAndPassword(string username, string password)
        {
            return _userRepository.GetByUsernameAndPassword(username, password);
        }

    }
}
